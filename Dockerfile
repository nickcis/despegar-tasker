FROM maven:3-jdk-8

COPY . /opt/tasker/

WORKDIR /opt/tasker

RUN mvn package -DskipTests

CMD ["java", "-jar", "target/tasker-0.0.1-SNAPSHOT.jar"]
