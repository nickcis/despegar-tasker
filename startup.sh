#!/bin/bash
# Tasker startup para Linux (y capaz MacOS X)

# Obtener carpeta local
dir="$( cd "$( dirname "$0" )" && pwd )"
pidfile="$dir/.tasker.pid"
tasker="$dir/target/tasker-0.0.1-SNAPSHOT.jar"
logfile="/dev/null"

function checkIfRunning() {
    if [ -f "$pidfile" ] && kill -0 $(cat "$pidfile") &>/dev/null
    then
        echo "Parece que Tasker ya esta corriendo"
        exit 1
    fi

    # Borramos el archivo de pid
    rm -f "$pidfile"
}

function start() {
    echo "Iniciando Tasker"
    if [ ! -f "$tasker" ]
    then
        echo "$tasker No existe"
        echo "Estas seguro de haber compilado? 'mvn package'"
        exit 1
    fi

    java -jar $tasker &> $logfile &
    local PID=$!
    disown

    echo -n "$PID" > $pidfile
}

function usage() {
    echo "Usage $0 [-l <log file>] [-h]" 1>&2
    exit 1
}

while getopts "hl:" o; do
    case "${o}" in
        l)
            logfile=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

checkIfRunning
start
