#!/bin/bash
# Tasker shutdown para Linux (y capaz MacOS X)

# Obtener carpeta local
dir="$( cd "$( dirname "$0" )" && pwd )"
pidfile="$dir/.tasker.pid"

function stop() {
    if [ ! -f "$pidfile" ] || ! kill -0 $(cat "$pidfile") &>/dev/null
    then
        echo "Tasker no esta corriendo"
        exit 1
    fi
    echo -n "Parando Tasker"
    kill -SIGTERM $(cat "$pidfile")
    rm -f "$pidfile"
    echo "...parado :)"
}

function usage() {
    echo "Usage $0 [-h]" 1>&2
    exit 1
}

while getopts "h:" o; do
    case "${o}" in
        *)
            usage
            ;;
    esac
done

stop
