package ar.com.nckweb.despegar.tasker.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doAnswer;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import ar.com.nckweb.despegar.tasker.model.Task;
import ar.com.nckweb.despegar.tasker.service.TaskService;
import ar.com.nckweb.despegar.tasker.repository.TaskRepository;
import ar.com.nckweb.despegar.tasker.controller.TaskController;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TaskControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TaskRepository repository;

	@MockBean
	private TaskService service;

	@Test
	public void createTaskShouldCallRepository() throws Exception {
		Task task = new Task(10L, Task.Status.WAITING);
		doAnswer(new Answer<Task>() {
			@Override
			public Task answer(InvocationOnMock invocationOnMock) throws Throwable {
				Task arg = (Task) invocationOnMock.getArguments()[0];
				assertEquals("When creating task status must be WAITING", task.getStatus(), Task.Status.WAITING);
				return task;
			}
		}).when(repository).save(any(Task.class));

		this.mockMvc.perform(post("/tasks").content("{}"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string(
				"{\n"+
				"  \"status\" : "+Task.Status.WAITING+",\n"+
				"  \"number\" : null,\n"+
				"  \"result\" : null,\n"+
				"  \"_links\" : {\n"+
				"    \"run\" : {\n"+
				"      \"href\" : \"http://localhost/tasks/"+task.getId()+"/run\"\n"+
				"    }\n"+
				"  }\n"+
				"}"
			));

		verify(repository, times(1)).save(any(Task.class));
	}

	@Test
	public void runTaskShouldCallService() throws Exception {
		Task task = new Task(10L, Task.Status.RUNNING, 4L);
		when(service.initTask(task.getId(), task.getNumber())).thenReturn(task);
		this.mockMvc.perform(
				put("/tasks/"+task.getId()+"/run")
					.contentType("application/json")
					.content(task.getNumber().toString())
			)
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string(
				"{\n"+
				"  \"status\" : " + Task.Status.RUNNING + ",\n"+
				"  \"number\" : " + task.getNumber() + ",\n"+
				"  \"result\" : null\n"+
				"}"
			));

		verify(service, times(1)).initTask(task.getId(), task.getNumber());
		verify(service, times(1)).runTask(any(Task.class));
	}
}
