package ar.com.nckweb.despegar.tasker.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.nckweb.despegar.tasker.model.Task;
import ar.com.nckweb.despegar.tasker.repository.TaskRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SyncTaskServiceTest {
	private TaskService service;

	@Before
	public void setUp() {
		this.service = new TaskService(this.repository, this.sleepService);
	}

	@MockBean
	private TaskRepository repository;

	@MockBean
	private SleepService sleepService;

	@Test
	public void fifthTaskShouldFail() throws Exception {
		when(repository.findByStatus(Task.Status.WAITING)).thenReturn(new ArrayList<Task>());

		ArrayList<Task> tasks = new ArrayList<>();
		for (long i=0; i < 5; i++) {
			Task task = new Task(i+1, Task.Status.WAITING);
			when(repository.findOne(task.getId())).thenReturn(task);
			when(repository.save(task)).thenReturn(task);
			tasks.add(task);

			TaskService.dispatchRunTask(service, task.getId(), 8L);
		}

		for (int i=0; i < 4; i++ ) {
			assertEquals("Task n: "+(i+1)+ " should succeed", tasks.get(i).getStatus(), Task.Status.FINISHED);
		}

		assertEquals("Task n: 5 should failed", tasks.get(4).getStatus(), Task.Status.FAILED);
	}
}
