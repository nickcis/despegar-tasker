package ar.com.nckweb.despegar.tasker.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.nckweb.despegar.tasker.model.Task;
import ar.com.nckweb.despegar.tasker.repository.TaskRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskServiceTest {
	@Autowired
	private TaskService service;

	@MockBean
	private TaskRepository repository;

	@MockBean
	private SleepService sleepService;

	@Test
	public void testPrime() {
		Long[] primes = new Long[] {2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L, 31L, 37L, 41L, 43L, 47L, 53L, 59L, 61L, 67L, 71L, 73L, 79L, 83L, 89L, 97L, 101L, 103L, 107L, 109L, 113L, 127L, 131L, 137L, 139L, 149L, 151L, 157L, 163L, 167L, 173L, 179L, 181L, 191L, 193L, 197L, 199L, 211L, 223L, 227L, 229L, 233L, 239L, 241L, 251L, 257L, 263L, 269L, 271L, 277L, 281L, 283L, 293L, 307L, 311L, 313L, 317L, 331L, 337L, 347L, 349L, 353L, 359L, 367L, 373L, 379L, 383L, 389L, 397L, 401L, 409L, 419L, 421L, 431L, 433L, 439L, 443L, 449L, 457L, 461L, 463L, 467L, 479L, 487L, 491L, 499L, 503L, 509L, 521L, 523L, 541L, 547L, 557L, 563L, 569L, 571L, 577L, 587L, 593L, 599L, 601L, 607L, 613L, 617L, 619L, 631L, 641L, 643L, 647L, 653L, 659L, 661L, 673L, 677L, 683L, 691L, 701L, 709L, 719L, 727L, 733L, 739L, 743L, 751L, 757L, 761L, 769L, 773L, 787L, 797L, 809L, 811L, 821L, 823L, 827L, 829L, 839L, 853L, 857L, 859L, 863L, 877L, 881L, 883L, 887L, 907L, 911L, 919L, 929L, 937L, 941L, 947L, 953L, 967L, 971L, 977L, 983L, 991L, 997L};

		for (Long prime : primes) {
			assertTrue("Number: " + prime + " is prime", service.isPrime(prime));
			prime = -prime;
			assertTrue("Number: " + prime + " is prime", service.isPrime(prime));
		}

		Long[] notPrimes = new Long[] {0L, 1L, 4L, 6L, 8L, 10L, 12L, 14L, 15L, 16L, 18L, 20L, 21L, 22L, 24L, 25L, 26L, 27L, 28L, 30L, 32L, 33L, 34L, 35L, 36L};
		for (Long notPrime : notPrimes) {
			assertFalse("Number: " + notPrime + " is not prime", service.isPrime(notPrime));
			notPrime = -notPrime;
			assertFalse("Number: " + notPrime + " is not prime", service.isPrime(notPrime));
		}
	}

	@Test
	public void runNotWaitingTaskShouldThrowError() {
		Task task = new Task(1L, Task.Status.RUNNING, 4L);
		when(repository.findOne(task.getId())).thenReturn(task);
		when(repository.findByStatus(Task.Status.RUNNING)).thenReturn(new ArrayList<Task>());

		boolean throwException = false;
		try {
			TaskService.dispatchRunTask(service, task.getId(), task.getNumber());
		} catch(IllegalStateException exception) {
			throwException = true;
		}

		assertTrue("dispatchRunTask should throw error if task is not Waiting", throwException);
	}

	@Test
	public void runNotExistingTaskShouldThrowError() {
		Task task = new Task(1L, Task.Status.RUNNING, 4L);
		when(repository.findOne(task.getId())).thenReturn(null);
		when(repository.findByStatus(Task.Status.RUNNING)).thenReturn(new ArrayList<Task>());

		boolean throwException = false;
		try {
			TaskService.dispatchRunTask(service, task.getId(), task.getNumber());
		} catch(IllegalArgumentException exception) {
			throwException = true;
		}

		assertTrue("dispatchRunTask should throw error if task there is no task with id", throwException);
	}

	@Test
	public void notMoreThanThreeTaskShouldRun() {
		Task task = new Task(1L, Task.Status.WAITING, 4L);
		ArrayList<Task> taskList = new ArrayList();

		taskList.add(task);
		taskList.add(task);
		taskList.add(task);

		when(repository.findOne(task.getId())).thenReturn(task);
		when(repository.findByStatus(Task.Status.RUNNING)).thenReturn(taskList);

		boolean throwException = false;
		try {
			TaskService.dispatchRunTask(service, task.getId(), task.getNumber());
		} catch(RuntimeException exception) {
			throwException = true;
		}

		assertTrue("dispatchRunTask should throw error if task there are already 3 task running", throwException);
	}

	@Test
	public void simpleRunTask() throws InterruptedException {
		Long number = 4L;
		Task task = new Task(1L, Task.Status.WAITING);
		when(repository.findOne(task.getId())).thenReturn(task);
		when(repository.findByStatus(Task.Status.RUNNING)).thenReturn(new ArrayList<Task>());
		when(repository.save(task)).thenReturn(task);

		TaskService.dispatchRunTask(service, task.getId(), number);
		assertEquals("Task must be returned in Running State", task.getStatus(), Task.Status.RUNNING);
		assertEquals("Task must be returned with the number setted", task.getNumber(), number);

		// XXX: Esto en realidad esta mal, al alcance de esta tarea no le veo sentido invertir el tiempo de investigar como desactivar el Async para el test.
		// Ya que el servicio de sleep esta mockeado, el tiempo que tarda en correr la tarea es lo que se tarde en despachar el _job_ en el thread pool, deberia ser _muy poco_
		Thread.sleep(100);

		verify(sleepService, times(1)).sleep(60000L);

		assertEquals("Task must in Finished State", task.getStatus(), Task.Status.FINISHED);
		assertFalse("Task must have correct result", task.getResult());
	}
}
