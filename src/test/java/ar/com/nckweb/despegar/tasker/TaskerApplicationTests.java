package ar.com.nckweb.despegar.tasker;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.nckweb.despegar.tasker.service.TaskService;
import ar.com.nckweb.despegar.tasker.controller.TaskController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskerApplicationTests {
	@Autowired
	private TaskController taskController;

	@Autowired
	private TaskService taskService;

	@Test
	public void contextLoads() {
		assertThat(taskController).isNotNull();
		assertThat(taskService).isNotNull();
	}
}
