import React from 'react';
import styles from './Page.css';
import Navbar from './Navbar';

const Page = ({children, onCreateTask, onRefresh}) => (
  <div>
    <Navbar onCreateTask={onCreateTask} onRefresh={onRefresh}/>
    <div className={styles.wrapper}>
      {children}
    </div>
  </div>
);

export default Page;
