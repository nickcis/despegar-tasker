import React from 'react';
import {Link} from 'react-router-dom';
import {Button} from "@blueprintjs/core";

import client from '../../client';

class Navbar extends React.Component {
  onCreateTask() {
    client.createTask()
      .then(r => {
        if (typeof(this.props.onCreateTask) == 'function')
          this.props.onCreateTask();
      });
  }

  render() {
    const onCreateTask = this.onCreateTask.bind(this);
    return (
      <nav className="pt-navbar pt-fixed-top">
        <div className="pt-navbar-group pt-align-left">
          <div className="pt-navbar-heading">Tasker</div>
        </div>
        <div className="pt-navbar-group pt-align-right">
          <Link to='/'><Button iconName="home" text="Home" className="pt-minimal" /></Link>
          <span className="pt-navbar-divider"></span>
          <Button iconName="add-to-artifact" className="pt-minimal" onClick={onCreateTask}/>
          <Button iconName="refresh" className="pt-minimal" onClick={this.props.onRefresh}/>
        </div>
      </nav>
    );
  }
}

export default Navbar;
