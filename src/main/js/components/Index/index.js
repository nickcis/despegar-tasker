import React from 'react';
import Page from '../Page';
import {Link} from 'react-router-dom';

import TaskTable from './TaskTable';

const Index = () => {
  let taskTable;
  const loadData = () => taskTable.loadData();
  return (
    <Page onCreateTask={loadData} onRefresh={loadData}>
      <TaskTable ref={ref => taskTable = ref}/>
    </Page>
  );
}

export default Index;
