import React from 'react';
import {Button} from "@blueprintjs/core";

import TaskDialog from './TaskDialog';

class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isRunDialogOpen: false
    };
  }

  getStatusIcon() {
    let iconName = "";
    switch(this.props.task.status) {
      case -1:
        iconName = 'cross';
        break;
      case 0:
        iconName = 'build';
        break;
      case 1:
        iconName = 'exchange';
        break;
      case 2:
        iconName = 'endorsed';
        break;
      default:
        iconName = 'help';
    }

    return (
      <span className={'pt-icon-standard pt-icon-' + iconName}/>
    );
  }

  handleOpenRunDialog() {
    this.setState({
      ...this.state,
      isRunDialogOpen: true,
    });
  }

  handleCloseRunDialog() {
    this.setState({
      ...this.state,
      isRunDialogOpen: false,
    });
  }

  handleRun() {
    if (typeof(this.props.onRun) == 'function')
      this.props.onRun();
  }

  getStatusString() {
    return this.props.task.statusString;
  }

  getResult() {
    return this.props.task.resultString;
  }

  getActions() {
    if (this.props.task.isRunnable()) {
      const handleOpenRunDialog = this.handleOpenRunDialog.bind(this);
      const handleCloseRunDialog = this.handleCloseRunDialog.bind(this);
      const handleRun = this.handleRun.bind(this);
      return [
        (<Button key="1" iconName="play" text="Ejecutar" onClick={handleOpenRunDialog}/>),
        (
          <TaskDialog
            key="2"
            task={this.props.task}
            isOpen={this.state.isRunDialogOpen}
            onClose={handleCloseRunDialog}
            onRun={handleRun} />
        )
      ];
    }
  }

  render() {
    return (
      <tr>
        <td>{this.getStatusIcon()}</td>
        <td>{this.getStatusString()}</td>
        <td>{this.getResult()}</td>
        <td>
          {this.getActions()}
        </td>
      </tr>
    );

  }
}

export default Task;
