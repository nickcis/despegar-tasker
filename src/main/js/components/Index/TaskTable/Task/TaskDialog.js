import React from 'react';
import {Button, Dialog, NumericInput} from "@blueprintjs/core";
import styles from './TaskDialog.css';

class TaskDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      value: undefined,
      error: undefined,
    };
  }

  handleRunTask(ev) {
    if (ev)
      event.preventDefault();

    if (typeof(this.state.value) == 'undefined' || !Number.isInteger(this.state.value)) {
      this.setState({
        ...this.state,
        error: "Debe ingresar un entero",
      });
      return;
    }

    this.setState({
      ...this.state,
      isLoading: true,
      error: undefined,
    });

    this.props.task.run(this.state.value)
      .then(r => {
        if (r.error) {
          this.setState({
            ...this.state,
            isLoading: false,
            error: `${r.error} - ${r.message}`,
          });
          return;
        }

        this.setState({
          ...this.state,
          isLoading: false,
        });

        if (typeof(this.props.onClose) == 'function')
          this.props.onClose();

        if (typeof(this.props.onRun) == 'function')
          this.props.onRun();
      });
  }

  handleValueChange(value) {
    this.setState({
      ...this.state,
      value,
    });
  }

  renderError() {
    if (this.state.error) {
      return (
        <div className={"pt-callout pt-intent-danger " + styles.errorCallout}>
          <h5>Error</h5>
          {this.state.error}
        </div>
      );
    }
  }

  render() {
    const handleRunTask = this.handleRunTask.bind(this);
    const handleValueChange = this.handleValueChange.bind(this);
    return (
      <Dialog
        title="Ejecutar tarea"
        isOpen={this.props.isOpen}
        onClose={this.props.onClose}
      >
        <form onSubmit={handleRunTask}>
          <div className="pt-dialog-body">
            {this.renderError()}
            <NumericInput
              className="pt-fill"
              allowNumericCharactersOnly={true}
              placeholder="Ingrese un n&uacute;mero"
              onValueChange={handleValueChange}
              value={this.state.value}/>
          </div>
          <div className="pt-dialog-footer">
            <div className="pt-dialog-footer-actions">
              <Button loading={this.state.isLoading} type="submit" className="pt-intent-primary">Ejecutar</Button>
            </div>
          </div>
        </form>
      </Dialog>
    );
  }
}

export default TaskDialog;
