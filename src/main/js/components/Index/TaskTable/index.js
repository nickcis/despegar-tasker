import React from 'react';
import {Spinner, Button} from "@blueprintjs/core";

import styles from './TaskTable.css';
import client from '~/client';

import Task from './Task';

class TaskTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        _embedded: {
          tasks: []
        },
        _links: {}
      }
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData(link) {
    this.setState({
      ...this.state,
      loading: true,
    });

    client.getTasks(link)
      .then(tasks => {
        this.setState({
          ...this.state,
          loading: false,
          data: tasks
        });
      });
  }

  head() {
    return (
      <thead>
        <tr>
          <td></td>
          <td>Estado</td>
          <td>Resultado</td>
          <td></td>
        </tr>
      </thead>
    );
  }

  getTask(task, i) {
    const handleRun = this.loadData.bind(this);
    return (
      <Task key={i} task={task} onRun={handleRun}/>
    );
  }

  emptyBody() {
    return (
      <tbody>
        <tr>
          <td colSpan="4" className={styles.emptyList}>
            <div className="pt-non-ideal-state">
              <div className="pt-non-ideal-state-visual pt-non-ideal-state-icon">
                <span className="pt-icon pt-icon-list"></span>
              </div>
              <h4 className="pt-non-ideal-state-title">No hay tareas</h4>
              <div className="pt-non-ideal-state-description">
                ¡Cre&aacute; una nueva tarea!.
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    );
  }

  body() {
    if (this.state.loading) {
      return (
        <tbody>
          <tr>
            <td className={styles.spinner} colSpan="4">
              <Spinner />
            </td>
          </tr>
        </tbody>
      );
    }

    const {tasks} = this.state.data._embedded;
    if (tasks.length <= 0)
      return this.emptyBody();

    return (
      <tbody>
        {tasks.map((t, i) => this.getTask(t, i))}
      </tbody>
    );
  }

  handleFooterClick(link) {
    this.loadData(link);
  }

  footer() {
    const handleFooterClick = link => this.handleFooterClick.bind(this, link);
    const {first, prev, next, last} = this.state.data._links;
    return (
      <tfoot>
        <tr>
          <td colSpan="4" className={styles.footer}>
            <div className="pt-button-group">
              <Button iconName="chevron-backward" disabled={!first} onClick={handleFooterClick(first)}/>
              <Button iconName="chevron-left" disabled={!prev} onClick={handleFooterClick(prev)}/>
              <Button iconName="chevron-right" disabled={!next} onClick={handleFooterClick(next)}/>
              <Button iconName="chevron-forward" disabled={!last} onClick={handleFooterClick(last)}/>
            </div>
          </td>
        </tr>
      </tfoot>
    );
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <table className={"pt-table "+styles.table}>
          {this.head()}
          {this.body()}
          {this.footer()}
        </table>
      </div>
    );
  }
}

export default TaskTable;
