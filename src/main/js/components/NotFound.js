import React from 'react';
import Page from './Page';

const NotFound = () => (
  <Page>
    404 Not found
  </Page>
);

export default NotFound;
