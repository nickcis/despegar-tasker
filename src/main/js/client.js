const Uris = {
  tasks: 'tasks',
  run: 'run',
  task: id => `${Uris.tasks}/${id}`,
  taskRun: id => `${Uris.task(id)}/run`,
};

class Task {
  constructor(task) {
    this._task = task;
  }

  get number() {
    return this._task.number;
  }

  get status() {
    return this._task.status;
  }

  get statusString() {
    switch(this.status) {
      case -1:
        return 'Fallida';
      case 0:
        return 'Esperando para ejecutar';
      case 1:
        return 'Ejecutando';
      case 2:
        return 'Finalizada'
      default:
        return 'Indefinido';
    }
  }

  get result() {
    return this._task.result;
  }

  get resultString() {
    if (this._task.status !== 2)
      return '-';
    return (this._task.result ? 'Es' : 'No es') + ` primo (${this._task.number})`;
  }

  isRunnable() {
    return 'run' in this._task._links;
  }

  run(number) {
    if (!this.isRunnable())
      throw new Error("Not runnable task");

    return client.runTask(this._task._links.run, number);
  }
}

class Client {
  get root() {
    return '/api';
  }

  fetch(url, {method, body, contentType}={}) {
    const opts = {
      method: method || 'GET',
      headers: {
        'Content-Type': contentType || 'application/json',
      }
    };

    if (body)
      opts.body = body;

    return fetch(url.startsWith('http') ? url : `${this.root}/${url}`, opts)
      .then(r => r.json());
  }

  getTask(id) {
    return this.fetch(id.href || Uris.task(id))
      .then(t => new Task(t));
  }

  runTask(id, number) {
    return this.fetch(id.href || Uris.taskRun(id), {
      method: 'PUT',
      body: ''+number,
    });
  }

  getTasks(link) {
    return this.fetch((link||{}).href || Uris.tasks)
      .then(json => {
        json._embedded.tasks = json._embedded.tasks.map(t => new Task(t));
        return json;
      });
  }

  createTask() {
    return this.fetch(Uris.tasks, {
      method: 'POST',
      body: '{}',
    });
  }
}

const client = new Client();

export default client;
