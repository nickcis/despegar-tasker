import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter} from 'react-router-dom'
import {Switch, Route} from 'react-router';

import 'normalize.css/normalize.css';
import '@blueprintjs/core/dist/blueprint.css';

import Index from './components/Index';
import NotFound from './components/NotFound';

const App = () => (
  <Switch>
    <Route path='/' component={Index} exact={true} />
    <Route component={NotFound} />
  </Switch>
);

ReactDOM.render((
  <HashRouter>
    <App/>
  </HashRouter>
), document.getElementById('root'));
