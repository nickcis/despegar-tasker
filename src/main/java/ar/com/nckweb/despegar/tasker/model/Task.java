package ar.com.nckweb.despegar.tasker.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Task {
	public static abstract class Status {
		public static final Integer WAITING = 0;
		public static final Integer RUNNING = 1;
		public static final Integer FINISHED = 2;
		public static final Integer FAILED = -1;
	}

	private @Id @GeneratedValue Long id;
	private Integer status = Status.WAITING;
	private Long number;
	private Boolean result;

	public Task() {}

	public Task(Long id, Integer Status) {
		this.id = id;
		this.status = status;
	}

	public Task(Long id, Integer status, Long number) {
		this.id = id;
		this.status = status;
		this.number = number;
	}

	public Task(Long id, Integer status, Long number, Boolean result) {
		this.id = id;
		this.status = status;
		this.number = number;
		this.result = result;
	}

	public Long getId() {
		return id;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String toString() {
		return "Task [ id: " + id + " status: " + status + " number: " + number + " result: " + result + " ]";
	}
}
