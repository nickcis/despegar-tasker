package ar.com.nckweb.despegar.tasker.component;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import ar.com.nckweb.despegar.tasker.model.Task;
import ar.com.nckweb.despegar.tasker.controller.TaskController;

// XXX: Construye mal el link de run. Bug:
// https://jira.spring.io/browse/DATAREST-972
@Component
public class TaskResourceProcessor implements ResourceProcessor<Resource<Task>> {
	@Override
	public Resource<Task> process(Resource<Task> resource) {
		if (Task.Status.WAITING.equals(resource.getContent().getStatus())) {
			resource.add(ControllerLinkBuilder.linkTo(
				ControllerLinkBuilder
					.methodOn(TaskController.class)
					.runTask(resource.getContent().getId(), null)
				)
				.withRel("run")
			);
		}
		return resource;
	}
}
