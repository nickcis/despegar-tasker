package ar.com.nckweb.despegar.tasker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

@Service
public class SleepService {
	/** Servicio que duerme el tiempo especificado.
	 *
	 * Se separa el _sleep_ para que sea mas facil mockearlo al realizar tests.
	 */
	public void sleep(long msec) throws InterruptedException {
		Thread.sleep(msec);
	}
}
