package ar.com.nckweb.despegar.tasker.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.nckweb.despegar.tasker.TaskerApplication;
import ar.com.nckweb.despegar.tasker.model.Task;
import ar.com.nckweb.despegar.tasker.repository.TaskRepository;

@Service
public class TaskService {
	private static final Logger logger = LoggerFactory.getLogger(TaskService.class);
	private static Long SLEEP_TIME = 60000L;
	private Long mockFail = 0L;

	private final TaskRepository repository;
	private final SleepService service;

	@Autowired
	public TaskService(TaskRepository repository, SleepService service) {
		this.repository = repository;
		this.service = service;
	}

	/** Chequea precondiciones sobre tarea y actualiza su estado a running.
	 *
	 * La tarea es guardada.
	 *
	 * @returns Task con estado actualizado
	 */
	public Task initTask(Long taskId, Long number) {
		Task task = repository.findOne(taskId);

		if (task == null) {
			String error = "No task with id " + taskId + " was found";
			logger.warn(error);
			throw new IllegalArgumentException(error);
		}

		if (! Task.Status.WAITING.equals(task.getStatus())) {
			String error = "A task was tried to be run in an invalid state " + task;
			logger.warn(error);
			throw new IllegalStateException(error);
		}

		List<Task> runningTasks = repository.findByStatus(Task.Status.RUNNING);

		if (runningTasks.size() >= TaskerApplication.MAX_RUNNING_TASKS) {
			String error = "Max running reached runningTask: " + runningTasks.size() + " MAX_RUNNING_TASKS: " + TaskerApplication.MAX_RUNNING_TASKS;
			logger.warn(error);
			throw new RuntimeException(error);
		}

		task.setNumber(number);
		task.setStatus(Task.Status.RUNNING);
		task = repository.save(task);

		return task;
	}

	/**
	 * Despacha tarea a ejecutar.
	 *
	 * Para que funcione los llamados Async, no se puede hacer llamados a metodos de su misma instancia.
	 * Para sobrepasar ese problema, se usa este metodo estatico.
	 *
	 * Primero chequea que Task exista y setea los valores de ella.
	 *
	 * @returns Task actualizada (RUNNING)
	 */
	public static Task dispatchRunTask(TaskService service, Long taskId, Long number) {
		Task task = service.initTask(taskId, number);
		service.runTask(task);
		return task;
	}

	/** Corre Task asincronicamente.
	 * La tarea debe tener seteado su _number_.
	 *
	 * Wrapper de funcion runTaskSync para atrapar cualquier error posible durante la ejecucion
	 */
	@Async
	public void runTask(Task task) {
		try {
			runTaskSync(task);
		} catch(Exception e) {
			logger.warn("runTaskSync(task: "+task+") has failed: "+e);
			task.setStatus(Task.Status.FAILED);
			repository.save(task);
		}
	}

	protected void runTaskSync(Task task) throws InterruptedException {
		logger.debug("Running async task: " + task);

		service.sleep(SLEEP_TIME);

		if (hasToFail()) {
			String text = "Forced failure. Task: "+task;
			logger.debug(text);
			throw new RuntimeException(text);
		} else {
			task.setStatus(Task.Status.FINISHED);
			task.setResult(isPrime(task.getNumber()));
		}

		repository.save(task);

		logger.info("Task " + task + " :: finished");
	}

	protected boolean hasToFail() {
		return (++mockFail) % 5 == 0;
	}

	public boolean isPrime(Long number) {
		if (number < 0)
			number = -number;

		if (number == 0 || number == 1)
			return false;

		for (Long i=2L; i < number; i++) {
			if (number % i == 0)
				return false;
		}

		return true;
	}

}
