package ar.com.nckweb.despegar.tasker.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import ar.com.nckweb.despegar.tasker.model.Task;

public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {
	List<Task> findByStatus(Integer status);

	@Override
	@RestResource(exported = false)
	public Task save(Task t);

	@Override
	@RestResource(exported = false)
	public void delete(Task t);
}
