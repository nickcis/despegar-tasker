package ar.com.nckweb.despegar.tasker;

import java.util.concurrent.Executor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableAsync
public class TaskerApplication {
	public static final int MAX_RUNNING_TASKS = 3;

	public static void main(String[] args) {
		SpringApplication.run(TaskerApplication.class, args);
	}

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(MAX_RUNNING_TASKS);
		executor.setMaxPoolSize(MAX_RUNNING_TASKS);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("Tasker-");
		executor.initialize();
		return executor;
	}
}
