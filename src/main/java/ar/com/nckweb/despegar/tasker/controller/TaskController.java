package ar.com.nckweb.despegar.tasker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;

import ar.com.nckweb.despegar.tasker.model.Task;
import ar.com.nckweb.despegar.tasker.repository.TaskRepository;
import ar.com.nckweb.despegar.tasker.service.TaskService;

@RepositoryRestController
@RequestMapping("/tasks")
public class TaskController {
	private static final Logger logger = LoggerFactory.getLogger(TaskController.class);

	private final TaskRepository repository;
	private final TaskService service;

	@Autowired
	public TaskController(TaskRepository repository, TaskService service) {
		this.repository = repository;
		this.service = service;
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Resource<Task> createTask() {
		return new Resource<Task>(repository.save(new Task()));
	}

	@RequestMapping(value = "{taskId}/run", method = RequestMethod.PUT)
	public @ResponseBody Resource<Task> runTask(@PathVariable Long taskId, @RequestBody Long number) {
		return new Resource<Task>(TaskService.dispatchRunTask(service, taskId, number));
	}
}
