const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  context: path.join(__dirname, 'src', 'main', 'js'),
  name: 'client',
  entry: {
    bundle: './App.js',
  },
  devtool: 'sourcemaps',
  output: {
    publicPath: '/built/',
    path: path.join(__dirname, 'src', 'main', 'resources', 'static', 'built'),
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: path.join(__dirname, '.'),
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
          presets: ['es2015', 'stage-0', 'react']
        }
      },
      {
        test: /\.(woff|woff2)$/,
        use: {
          loader: 'url-loader',
          options: {
            name: 'fonts/[hash].[ext]',
            limit: 5000,
            mimetype: 'application/font-woff'
          }
        }
      },
      {
        test: /\.(ttf|eot|svg)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'fonts/[hash].[ext]'
          }
        }
      },
      {
        test: /node_modules\/.*?\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader'
        }),
      },
      {
        test: /\.css$/,
        exclude: path.resolve('node_modules'),
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: {
            loader: 'css-loader',
            query: {
              localIdentName: '[name]__[local]___[hash:base64:5]',
              modules: true
            }
          },
        }),
      }
    ]
  },
  plugins: [
    new webpack.NormalModuleReplacementPlugin(/^~/, resource => {
      resource.request = resource.request.replace(/^~/, module.exports.context);
    }),
    new ExtractTextPlugin({
      filename: '[name].css',
      allChunks: true
    }),
  ]
};
