# Tasker

Correr aplicación:

```bash
$ mvn spring-boot:run
```

Empaquetar:

```bash
$ mvn package
# Para ejecutar
$ java -jar target/tasker-0.0.1-SNAPSHOT.jar
```

Correr en Docker:

```bash
$ docker build -t nickcis/tasker .
$ docker run -ti --rm -p 8080:8080 nickcis/tasker
```

Created from [Spring Initializer](http://start.spring.io/)


## Tecnologías Utilizadas

* Spring (SpringBoot, Rest, Repository)
* H2 Memory Database
* ReactJS:
  * Webpack + Babel: Para bundle y transpiling
  * React Router
  * BlueprintJS para componentes

## Ejecutar como demonio

Se encuentran los scripts `startup.sh` y `shutdown.sh` para poder ejecutar la aplicación como demonio:

```bash
$ ./startup.sh -h
Usage ./startup.sh [-l <log file>] [-h]
```

El parámetro `-l` servirá para utilizar un archivo de log, por defecto, el log de la aplicación es descartado.

```bash
$ ./startup.sh -l log
Iniciando Tasker
$ tail log
2017-08-14 05:17:08.383  INFO 21464 --- [           main] o.s.d.r.w.RepositoryRestHandlerMapping   : Mapped "{[/api/{repository}/search/{search}],methods=[OPTIONS],produces=[application/hal+json || application/json]}" onto public org.springframework.http.ResponseEntity<java.lang.Object> org.springframework.data.rest.webmvc.RepositorySearchController.optionsForSearch(org.springframework.data.rest.webmvc.RootResourceInformation,java.lang.String)
2017-08-14 05:17:08.383  INFO 21464 --- [           main] o.s.d.r.w.RepositoryRestHandlerMapping   : Mapped "{[/api/{repository}/search/{search}],methods=[HEAD],produces=[application/hal+json || application/json]}" onto public org.springframework.http.ResponseEntity<java.lang.Object> org.springframework.data.rest.webmvc.RepositorySearchController.headForSearch(org.springframework.data.rest.webmvc.RootResourceInformation,java.lang.String)
2017-08-14 05:17:08.386  INFO 21464 --- [           main] o.s.d.r.w.BasePathAwareHandlerMapping    : Mapped "{[/api/profile/{repository}],methods=[GET],produces=[application/schema+json]}" onto public org.springframework.http.HttpEntity<org.springframework.data.rest.webmvc.json.JsonSchema> org.springframework.data.rest.webmvc.RepositorySchemaController.schema(org.springframework.data.rest.webmvc.RootResourceInformation)
2017-08-14 05:17:08.388  INFO 21464 --- [           main] o.s.d.r.w.BasePathAwareHandlerMapping    : Mapped "{[/api/profile/{repository}],methods=[GET],produces=[application/alps+json || */*]}" onto org.springframework.http.HttpEntity<org.springframework.data.rest.webmvc.RootResourceInformation> org.springframework.data.rest.webmvc.alps.AlpsController.descriptor(org.springframework.data.rest.webmvc.RootResourceInformation)
2017-08-14 05:17:08.388  INFO 21464 --- [           main] o.s.d.r.w.BasePathAwareHandlerMapping    : Mapped "{[/api/profile/{repository}],methods=[OPTIONS],produces=[application/alps+json]}" onto org.springframework.http.HttpEntity<?> org.springframework.data.rest.webmvc.alps.AlpsController.alpsOptions()
2017-08-14 05:17:08.389  INFO 21464 --- [           main] o.s.d.r.w.BasePathAwareHandlerMapping    : Mapped "{[/api/profile],methods=[OPTIONS]}" onto public org.springframework.http.HttpEntity<?> org.springframework.data.rest.webmvc.ProfileController.profileOptions()
2017-08-14 05:17:08.389  INFO 21464 --- [           main] o.s.d.r.w.BasePathAwareHandlerMapping    : Mapped "{[/api/profile],methods=[GET]}" onto org.springframework.http.HttpEntity<org.springframework.hateoas.ResourceSupport> org.springframework.data.rest.webmvc.ProfileController.listAllFormsOfMetadata()
2017-08-14 05:17:08.632  INFO 21464 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2017-08-14 05:17:08.714  INFO 21464 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 8080 (http)
2017-08-14 05:17:08.722  INFO 21464 --- [           main] a.c.n.despegar.tasker.TaskerApplication  : Started TaskerApplication in 7.991 seconds (JVM running for 8.554)
```

Para parar la aplicación basta con ejecutar `shutdown.sh`

```bash
$ ./shutdown.sh
Parando Tasker...parado :)
```

## Como utilizar la interfaz

Si se corre localmente, se deberá entrar a la URL: [`http://localhost:8080`](http://localhost:8080).

![Pantalla inicial](./images/1.png)

Inicialmente se verá la lista de tareas vacía.

### Crear una nueva Tarea

Para crear una tarea hay que hacer click sobre el ícono de _Crear Tarea_ ubicado en la esquina superior derecha de la pantalla.

![Crear una tarea](./images/2.png)

Automáticamente se refrescará la lista de tareas y se podrá ver la nueva tarea creada en estado _Esperando para ejecutar_.

![Nueva tarea](./images/3.png)

### Refrescar lista de tareas

Para refrescar la lista de tareas hay que hacer click sobre el ícono de _Refrescar_ ubicado en la esquina superior derecha de la pantalla.

![Refrescar lista](./images/4.png)

### Ejecutar una tarea (manualmente)

Para ejecutar una tarea se debe hacer click en el botón _Ejecutar_ ubicado sobre la fila de la tarea que queremos ejecutar.

![Ejecutar tarea](./images/5.png)

Se abrirá una ventana donde debemos ingresar el número que deseemos.

![Ingresar número](./images/6.png)

Posteriormente se deberá hacer click en _Ejecutar_.

![Ejecutar](./images/7.png)

Finalmente, se cerrará la ventana y la lista de tareas se actualizará automaticamente.

![Lista](./images/8.png)

## Consideraciones

* Ejecutar una tarea es asincrónico, es decir, el _request HTTP_ es devuelto y el servidor ejecuta la tarea en _background_.
* Al llegar a la limitante de 3 tareas en simultaneo, si se intenta ejecutar una nueva tarea, se devuelve error por interfaz, pero no se considera que esa tarea haya fallado.
* Se utiliza una Base de Datos H2 para los datos en memoria, para chequear las tareas que se encuetran corriendo, se consulta en esta base de datos las tareas que esten en estado _Running_, para el alcance del ejercicio se supone que esta base de datos no va a entrar en un estado inconsistente (es decir, no va a haber una tarea con estado _Running_ sin que esta se encuentre corriendo).
* `TaskService` controla precondiciones y arroja errores si la tarea se encuentra en estado inválido (Tarea inexistente, Tarea no en estado WAITING, etc), se utiliza el manejador de Errores de _Spring_ por defecto, se debería modificar esto para que al cliente le llegue un error con código `4xx` y no `500`.
* Se separa en carpetas por funcionalidades para ser coherentes con lo que sería una organización de archivos de un proyecto de mayor tamaño. Como el exámen tiene un alcance corto la mayoría de las carpetas tienen un solo archivo, lo que hace realmente no sea muy cómoda esta organización.
* No se implementa ningún tipo de seguridad (autenticación, etc) ya que el enunciado no lo pide.
* Se uso _React Router_ por un tema de costumbre, pero la realidad es que no se hicieron varias páginas como para justificar su uso.
* Si se pretender que la _App_ crezca, se debería considerar utilizar un manejador de estado (ej: _Redux_)
* Actualmente para que se tomen los cambios de _React_ es necesario reiniciar el servidor, se debería buscar una manera para que no haga falta esto.
* Actualmente no se esta _minificando_ javascript, css. Además, se que esta incluyendo el _sourcemap_, se debería dar la opción de _producción_ donde se hagan estas cosas.
* Por un tema de tiempos, se decidió no realizar test de automatización (es decir, simular un navegador para probar la app, ej: _PhantomJs_, _Selenium_, etc)
* Es necesario recargar manualmente la interfaz para ver si finalizó o falló una tarea.
* Ya que el enunciado no lo pedía, las tareas solo guardan su estado, número y resultado. No se agregó ningún tipo de texto (título, etc), ni tiempo de cuando fueron creadas.
